import React, { Component } from 'react';
import './App.css';
import { Button } from 'element-react';
import 'element-theme-default';
import axios from 'axios';

class App extends Component {

  componentDidMount(){
    axios.get('/api/users').then(result=>{
      console.log(result.data);
    });
  }

  render() {
    return (
      <div className="App">
        <Button type="primary">Hello</Button>
      </div>
    );
  }
}

export default App;
